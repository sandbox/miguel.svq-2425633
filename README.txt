INTRODUCTION
------------
The Webformclass module adds a new formatter providing an easy way to
add CSS classes to webform forms.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/miguel.svq/2425633


 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2425633


OVERVIEW
--------
Adds a new formatter to allow webforms to set CSS classes in the FORM tag,
avoiding the pain of styling based on form ids that can change between 
development/production sites and making easier to reuse styles across 
different forms.

The module also adds an extra class to the form with the view mode:
e.g. webformclass-full, webformclass-teaser, etc.

It is a minimal module: implement the formater to ensure CSS class names
correctness and a single hook to make the work.

USAGE
-----
Add a text field (single value) to your webform content type and select 
"Webform CSS Classes" as formater in the displays.

The text entered as field value will be added to the form class attribute.
You can set several class names separating them with spaces. The values are
filtered using drupal drupal_html_class(), so they can differ from your input.
Avoid surprises using just lowercase letters, numbers and hyphens.


REQUIREMENTS
------------
This module requires the following modules:
 * Webform (https://www.drupal.org/project/webform)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
The module has no menu or modifiable settings.


MAINTAINERS
-----------
Current maintainers:
 * Miguel Pérez (miguel.svq) - https://www.drupal.org/user/866382
